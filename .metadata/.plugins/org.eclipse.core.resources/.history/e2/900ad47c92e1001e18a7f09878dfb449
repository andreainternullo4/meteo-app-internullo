package it.meteo.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import java.security.*;
import java.time.LocalDateTime;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import it.meteo.dto.UtenteLoginRequestDto;
import it.meteo.dto.UtenteLoginResponseDto;
import it.meteo.dto.UtenteRegistrazioneDto;
import it.meteo.model.Utente;
import it.meteo.service.UtenteService;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/utente")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UtenteController {
	
	@Autowired
	private UtenteService utenteService;

	@POST
	@Path("/registrazione")
	public Response userRegistration(@Valid @RequestBody UtenteRegistrazioneDto utente) {
		
		try {
			
			// Validazione password
			if( !Pattern.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,20}", utente.getPassword()) ) {
							
				return Response.status(Response.Status.BAD_REQUEST).build();
			
			}
			
			// Verifico se esiste già l'utente attraverso l'email
			if( utenteService.existsUtenteByEmail(utente.getEmail())) {
							
				return Response.status(Response.Status.BAD_REQUEST).build();
			
			}
			
			utenteService.userRegistration(utente);
			return Response.status(Response.Status.OK).build();
			
		} catch (Exception e) {
			
			return Response.status(Response.Status.BAD_REQUEST).build();
			
		}
	}
	
	@POST
	@Path("/login")
	public Response userLogin(@RequestBody UtenteLoginRequestDto utente) {
		
		try {
			
			if( utenteService.loginUtente(utente) ) {
				// Se il login è true, le credenziali sono valide e di conseguenza
				// dobbiamo fare tornare il token: lo inserisco nel corpo della risposta
				UtenteLoginResponseDto utenteDto = issueToken(utente.getEmail());
				return Response.ok(utenteDto).build();
			}
			// Se non passa, ritorno che la richiesta è sbagliata
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
	}
	
	public UtenteLoginResponseDto issueToken(String email) {
		
		byte[] secret = "stringachenessunodevesapere11111111".getBytes();
		
		Key chiaveSegreta = Keys.hmacShaKeyFor(secret);
		
		Utente infoUtente = utenteService.findByEmail(email);
		
		Map<String, Object> map = new HashMap<>();
		
		map.put("nome", infoUtente.getNome());
		map.put("cognome", infoUtente.getCognome());
		map.put("email", email);
		map.put("ruolo", infoUtente.getRuolo().getTipologia());
		
		Date creation = new Date();
		
		Date end = java.sql.Timestamp.valueOf(LocalDateTime.now().plusMinutes(15L));
		
		String jwtToken = Jwts.builder()
								.setClaims(map)
								.setIssuer("http://localhost:8080") // Chi emette il token = il server
								.setIssuedAt(creation) // Data di creazione del token
								.setExpiration(end) // Data fine validità del token
								.signWith(chiaveSegreta) // Chiave segreta per firmare
								.compact();
		
		UtenteLoginResponseDto token = new UtenteLoginResponseDto();
		token.setToken(jwtToken);
		token.setTokenCreationTime(creation);
		token.setTtl(end);
		
		return token;
	}
}
