import Cookies from "js-cookie"
import { URLs, cookieTypes, jwtExpirations } from "./config/rest-service-config"
import { jwtDecode } from "jwt-decode"

//registrazione
export async function registerUser(formData){
    const jsonData = JSON.stringify(formData)
    const response = await fetch(URLs.registerUrl, {
      mode: "cors",
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: jsonData
    })
    return response.status
  }

//login
export async function loginUser(formData){
    const jsonData = JSON.stringify(formData)
    const response = await fetch(URLs.loginUrl, {
      mode: "cors",
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: jsonData
    })
    return response
}

//salvo il jwt nei cookies
export function setLoginCookie(jwtToken) {
    const jwtString = JSON.stringify(jwtToken.token)
    Cookies.set(cookieTypes.jwt, jwtString, {expires: jwtExpirations.oneMonth})
    return jwtDecode(jwtString)
}

//cancello il cookie
export function deleteCookie(type){
    Cookies.remove(type)
}

//controllo che esista il jwt e, se esiste, lo decodifico per leggere i dati dell'utente
export function checkLoginCookie(type){
    const loginCookie = Cookies.get(type)
    if(loginCookie != undefined){
      return jwtDecode(loginCookie)
    } else {
      return null
    }
}

export async function acquisisciLocalita(localita){
    const response = await fetch(URLs.getLanAndLon + `?q=${localita}&limit=1&appid=fcd483dfc9f256bd32a9a58965865657`, {
        mode: "cors",
        method: "GET"
    })
    //console.log( await response.json())
    if(response.ok){
        const latAndLon = await response.json()
        return latAndLon[0];
    }
    return null;
}

export async function acquisisciInformazioni(lat, lon){
    const response = await fetch(URLs.getInformation + `?lat=${lat}&lon=${lon}&exclude=minutely,hourly,daily&units=metric&appid=fcd483dfc9f256bd32a9a58965865657`, {
        mode: "cors",
        method: "GET"
    })
    if(response.ok){
        const information = await response.json()
        //console.log(information)
        return information;
    }
    return null;
}

export async function salvaInformazioni(informazioni) {
    const jsonData = JSON.stringify(informazioni)
    const response = await fetch(URLs.salvaInfo, {
        mode: "cors",
        method: "POST",
        body: jsonData,
        headers: {
            "Content-Type": "application/json"
        },
  })
  return response.status
}