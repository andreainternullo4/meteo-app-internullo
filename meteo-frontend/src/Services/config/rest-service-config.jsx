export const URLs = {
    registerUrl: 'http://localhost:8080/api/utente/registrazione',
    loginUrl: 'http://localhost:8080/api/utente/login',
    getLanAndLon: 'http://api.openweathermap.org/geo/1.0/direct',
    getInformation: 'https://api.openweathermap.org/data/3.0/onecall',
    salvaInfo: 'http://localhost:8080/api/informazioni/aggiungiInformazioni',
    getUserbyEmail: 'http://localhost:8080/api/utente/utente',
}

export const jwtExpirations = {
    oneYear: new Date().getFullYear(),
    oneMonth: 31,
    oneWeek: 7,
    oneDay: 1
}
  
export const userRoles = {
    Admin: 1,
    Utente: 2
}
  
export const cookieTypes = {
    jwt: "JWT"
}