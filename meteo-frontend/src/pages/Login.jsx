import { useContext, useEffect } from "react";
import { AuthContext } from "../contexts/AuthContext";
import LoginForm from "../components/LoginForm/LoginForm";

function Login() {
    const {setCurrentUser} = useContext(AuthContext)

    useEffect(() => {
        document.title = "login";
    }, []);

    return (  
        <>
            <LoginForm setCurrentUser={setCurrentUser}></LoginForm>
        </>
    );
}

export default Login;