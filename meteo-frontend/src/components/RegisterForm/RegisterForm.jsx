import { useEffect, useState } from "react";
import { CheckPasswordMatch, ValidatePassword, areValuesMatching, regexMatch } from "../../Services/ValidationService";
import logo from "../../assets/openweather.svg"
import { Link } from "react-router-dom";
import { registerUser } from "../../Services/RESTService";


function RegisterForm() {

    const [users, setUsers] = useState([]);

    const [formData, setFormData] = useState({
        nome: "",
        cognome: "",
        email: "",
        password: "",
    });

    const [responseStatus, setResponseStatus] = useState(null);

    const [confirmPassword, setConfirmPassword] = useState("");

    const [valuesMatch, setValuesMatch] = useState(null);

    const [validity, setValidity] = useState(null);

    const handleFocus = () => {
        if (!validity) {
          setValidity(null);
        }
        if (!valuesMatch) {
          setValuesMatch(null);
        }
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };
    
      const handleConfirmPasswordChange = (e) => {
        setConfirmPassword((confirm) => (confirm = e.target.value));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (CheckPasswordMatch(formData.password, confirmPassword)) {
          setValuesMatch(true);
          if (ValidatePassword(formData.password)) {
            setValidity(true);
            setUsers([...users, formData]);
            const responseStatus = await registerUser(formData);
            setResponseStatus(responseStatus);
          } else {
            setValidity(false);
            setValuesMatch(null);
          }
        } else {
          setValuesMatch(false);
        }
        setFormData({
          nome: "",
          cognome: "",
          email: "",
          password: "",
        });
        setConfirmPassword((confirm) => (confirm = ""));
    };

    useEffect(() => {
        if (users.length > 0) {
          setConfirmPassword((confirm) => (confirm = ""));
        }
    }, [users]);
    
    const handleResetClick = () => {
        setFormData({
          nome: "",
          cognome: "",
          email: "",
          password: "",
        });
        setConfirmPassword((confirm) => (confirm = ""));
        setValidity(null);
    };

    return (  
        <>
            <div className="px-4 py-5 px-md-5 text-center justify-content-center align-items-center text-lg-start d-flex" style={{backgroundColor: "hsl(0, 0%, 96%)", height: "100vh"}}>
            <div className="container">
                <div className="row gx-lg-5 align-items-center">
                    <div className="col-lg-6 mb-5 mb-lg-0">
                        <img src={logo} alt="Logo" className="rounded mx-auto d-block h-30"></img>
                    </div>
                    <div className="col-lg-6 mb-5 mb-lg-0">
                        <div className="card">
                            <div className="card-body py-5 px-md-5">
                                <form onSubmit={handleSubmit}>
                                    <div className="row">
                                        <div className="col-md-6 mb-4">
                                            <div className="form-outline">
                                                <label htmlFor="exampleInputFirstName1" className="form-label">Nome</label>
                                                <input 
                                                    name="nome" 
                                                    className="form-control" 
                                                    id="FirstName" 
                                                    aria-describedby="firstNameHelp" 
                                                    placeholder="Nome"
                                                    value={formData.nome}
                                                    onChange={handleChange}
                                                    onFocus={handleFocus}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-6 mb-4">
                                            <div className="form-outline">
                                                <label htmlFor="exampleInputLastName1" className="form-label">Cognome</label>
                                                <input 
                                                    name="cognome"
                                                    value={formData.cognome}
                                                    type="text"
                                                    className="form-control"
                                                    id="lastName"
                                                    placeholder="Cognome"
                                                    onChange={handleChange}
                                                    onFocus={handleFocus}
                                                />
                                            </div>
                                        </div>
                                    </div>
                            
                                    <div className="form-outline mb-4">
                                        <label htmlFor="exampleInputEmail1" className="form-label">Email</label>
                                        <input 
                                            name="email"
                                            value={formData.email}
                                            type="email"
                                            className="form-control"
                                            id="email"
                                            placeholder="Indirizzo email"
                                            onChange={handleChange}
                                            onFocus={handleFocus}
                                        />
                                        <div id="emailHelp" className="form-text">Non condivideremo la tua email</div>
                                    </div>
                                    <div className="form-outline mb-4">
                                        <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                        <input 
                                            name="password"
                                            value={formData.password}
                                            type="password"
                                            className="form-control"
                                            id="password"
                                            placeholder="Password"
                                            onChange={handleChange}
                                            onFocus={handleFocus}
                                        />
                                    </div>
                                    <div className="form-outline mb-4">
                                        <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                        <input 
                                            name="confirmPassword"
                                            value={confirmPassword}
                                            type="password"
                                            className="form-control"
                                            id="confirmPassword"
                                            placeholder="Conferma password"
                                            onChange={handleConfirmPasswordChange}
                                            onFocus={handleFocus}
                                        />
                                    </div>
                                    {areValuesMatching(valuesMatch)}
                                    {regexMatch(validity)}
                                    <div className="d-flex flex-column align-items-center">
                                        <div className="d-flex flex-column align-items-center justify-content-center">
                                            <button type="submit" className="btn btn-primary my-3">
                                                Registrati
                                            </button>
                                            <button
                                                type="reset"
                                                className="btn btn-primary col-12"
                                                onClick={handleResetClick}
                                            >
                                                Reset
                                            </button>
                                        </div>
                                        <Link to="/login">
                                            Sei già registrato?
                                        </Link>
                                        {responseStatus == null && <></>}
                                        {responseStatus == 201 && (
                                            <div className="mt-1" style={{ color: "green", textAlign: 'center'  }}>Ti sei registrato correttamente</div>
                                        )}
                                        {responseStatus != null && responseStatus != 201 && (
                                            <div className="mt-1" style={{ color: "red", textAlign: 'center' }}>
                                                Errore, prova a registrarti di nuovo
                                            </div>
                                        )}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/*<Footer></Footer>*/}
        </>
    );
}

export default RegisterForm;