import { loginUser, setLoginCookie } from "../../Services/RESTService";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import logo from "../../assets/openweather.svg"

function LoginForm({setCurrentUser}) {
    const [formData, setFormData] = useState({ email: "", password: ""});

    const [loginStatus, setLoginStatus] = useState(true);

    const navigateTo = useNavigate();

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const response = await loginUser(formData);
        if (response.status == 200) {
          const jwtToken = await response.json();
          const decoded = setLoginCookie(jwtToken);
          setCurrentUser({
            id: decoded.id,
            nome: decoded.nome,
            cognome: decoded.cognome,
            email: decoded.email,
            ruoli: decoded.ruoli,
          });
          setLoginStatus(true);
          navigateTo("/main");
        } else {
          setLoginStatus(false);
        }
      };

    return (  
        <>
        <div className="container-fluid px-4 py-5 px-md-5 text-center text-lg-start d-flex align-items-center" style={{backgroundColor: "hsl(0, 0%, 96%)", height: "100vh"}}>
            <div className="container">
                <div className="row gx-lg-5 align-items-center">
                    <div className="col-lg-6 mb-5 mb-lg-0">
                        <img src={logo} alt="Logo" className="rounded mx-auto d-block h-30"></img>
                    </div>
                    <div className="col-lg-6 mb-5 mb-lg-0">
                        <div className="card">
                            <div className="card-body py-5 px-md-5">
                                <form onSubmit={handleSubmit}>
                                    <div className="form-outline mb-4">
                                        <label htmlFor="exampleInputEmail1" className="form-label">Email</label>
                                        <input 
                                            type="email" 
                                            name="email" 
                                            className="form-control" 
                                            id="exampleInputEmail1" 
                                            aria-describedby="emailHelp" 
                                            placeholder="email" 
                                            value={formData.email}
                                            onChange={handleChange}
                                        />
                                    </div>
                                    <div className="form-outline mb-4">
                                        <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                        <input 
                                            type="password" 
                                            name="password" 
                                            className="form-control" 
                                            id="exampleInputPassword1" 
                                            placeholder="password" 
                                            value={formData.password}
                                            onChange={handleChange}
                                        />
                                    </div>
                                    <div className="d-flex flex-column align-items-center">
                                        <div>
                                            <button type="submit" className="btn btn-primary">
                                                Login
                                            </button>
                                        </div>
                                        {!loginStatus ? (
                                            <div className="mt-1">
                                                <div style={{ color: "red" }}>Errore durante il login, riprova</div>
                                            </div>
                                            ) : (
                                            <></>
                                        )}
                                        <Link className="alreadyRegisteredLink mt-2" to="/registrazione">
                                            Non sei ancora registrato?
                                        </Link>
                                    </div>
                                </form>                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    );
}

export default LoginForm;