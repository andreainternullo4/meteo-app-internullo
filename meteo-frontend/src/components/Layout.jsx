import { useOutlet } from "react-router-dom";
import SimpleSection from "./SimpleSection";
import Navbar from "./Navbar/Navbar";
//import Footer from "./Footer/Footer"

export default function Layout() {
  const outlet = useOutlet();

  return (
    //layout come componente estremamente riutilizzabile
    <div>
      <Navbar></Navbar>
      <SimpleSection>{outlet}</SimpleSection>
      {/*<Footer></Footer>*/}
    </div>
  );
}