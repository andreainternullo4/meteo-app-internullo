import { useState } from "react";
import logo from "../../assets/openweather.svg"
import { acquisisciInformazioni, acquisisciLocalita, checkLoginCookie, salvaInformazioni } from "../../Services/RESTService";
import Cookies from "js-cookie";
import { jwtDecode } from "jwt-decode";
import { cookieTypes } from "../../Services/config/rest-service-config";

function InserisciLocalitaForm() {

    const [localita, setLocalita] = useState('')

    const [daticaricati, setDaticaricati] = useState('')

    const handleChange = (e) => {
        setLocalita((valore) => (valore = e.target.value));
    };

    const [informazioni, setInformazioni] = useState({
        nomecitta:"",
        stato: "",
        regione: "",
        gradi: "",
        gradiPercepiti: "",
        umidita: "",
        descrizione: "",
        visibilita: "",
        uv: "",
        pressione: "",
        vento: "",
    })

    const handleSubmit = async (e) => {
        e.preventDefault();
        const response = await acquisisciLocalita(localita);
        if(response !== null) {
            const lat = response.lat;
            const lon = response.lon;

            console.log(response)

            const response2 = await acquisisciInformazioni(lat, lon);
            if(response2 !== null) {
                setInformazioni({
                    nomecitta: response.name,
                    stato: response.country,
                    regione: response.state,
                    gradi: response2.current.temp,
                    gradiPercepiti: response2.current.feels_like,
                    umidita: response2.current.humidity,
                    descrizione: response2.current.weather[0].description,
                    visibilita: response2.current.visibility,
                    uv: response2.current.uvi,
                    pressione: response2.current.pressure,
                    vento: response2.current.wind_speed
                })
                setDaticaricati(true)
                const decoded = checkLoginCookie(cookieTypes.jwt)
                console.log(decoded.email)
                
            }
            const response3 = await salvaInformazioni(informazioni);
            console.log(response3)
        }
    }

    return (  
        <>
        <div className="container-fluid px-4 py-5 px-md-5 text-center text-lg-start d-flex align-items-center" style={{backgroundColor: "hsl(0, 0%, 96%)", height: "100vh"}}>
            <div className="container">
                <div className="row gx-lg-5 align-items-center">
                    <div className="col-lg-12 mb-5 mb-lg-0">
                        <img src={logo} alt="Logo" className="rounded mx-auto d-block h-30"></img>
                    </div>
                    <form className="mt-5" onSubmit={handleSubmit}>
                    <div className="row">                        
                        <div className="col-lg-10 mb-5 mb-lg-0">
                            <input type="text" 
                                    id="inputLocalita" 
                                    className="form-control" 
                                    aria-describedby="LocalitaHelpBlock"
                                    placeholder="Roma, Londra, Milano..."
                                    value={localita}
                                    onChange={handleChange}
                            />
                            <small id="LocalitaHelpBlock" className="form-text text-muted">
                                Digita qui sopra il nome della località interessata.
                            </small>
                        </div>
                        <div className="col-lg-2 mb-5 mb-lg-0">
                            <div className="d-flex flex-column align-items-center">
                                <div>
                                    <button type="submit" className="btn btn-primary">
                                        Cerca
                                    </button>
                                </div>
                            </div> 
                        </div>
                    </div> 
                    <div>{daticaricati ? (
                            <>
                                <div className="row mt-4">
                                    <div className="col-lg-6 mb-lg-0" style={{backgroundColor: "white", borderRadius: "15px", boxShadow: "10px  10px 5px #dedede,-10px -10px 5px #dedede,10px -10px 5px #dedede,-10px  10px 5px #dedede"}}>
                                        <div className="row">
                                            <div className="col-lg-12 mt-2 mb-lg-0 font-weight-bold"><h4 className="font-weight-bold">{informazioni.nomecitta}, {informazioni.regione}, {informazioni.stato}</h4></div>
                                            <div className="col-lg-12 mb-lg-0 font-weight-bold"><h2>{informazioni.gradi}°C</h2></div>
                                            <div className="col-lg-12 mb-5 mb-lg-0 font-weight-bold"><p>gradi percepiti {informazioni.gradiPercepiti}°C, {informazioni.descrizione}</p></div>
                                            <div className="row">
                                                <div className="col-lg-4 mb-5 mb-lg-0"><p>{informazioni.vento} m/s</p></div>
                                                <div className="col-lg-4 mb-5 mb-lg-0"><p>{informazioni.pressione} hPa</p></div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-4 mb-5 mb-lg-0"><p>Umidita: {informazioni.umidita}% </p></div>
                                                <div className="col-lg-4 mb-5 mb-lg-0"><p>UV: {informazioni.uv}</p></div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-6 mb-5 mb-lg-0"><p>Visibilità: {informazioni.visibilita}m </p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </>
                        ) : (<></>)}
                    </div>
                    </form>            
                </div>
            </div>
        </div>
        </>
    );
}

export default InserisciLocalitaForm;