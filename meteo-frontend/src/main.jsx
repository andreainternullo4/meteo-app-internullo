import React from 'react'
import ReactDOM from 'react-dom/client'
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import "../node_modules/bootstrap-icons/font/bootstrap-icons.css";
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import AuthContextProvider from './contexts/AuthContextProvider.jsx';
import Layout from './components/Layout.jsx';
import Home from './pages/Home.jsx';
import Login from './pages/Login.jsx';
import Register from './pages/Register.jsx';
import Main from './pages/Main.jsx';

const router = createBrowserRouter(
  [
    {
      element: <AuthContextProvider><Layout/></AuthContextProvider>,
      children: [
        {
          path: "/",
          element: <Home/>
        },
        {
          path: "login",
          element: <Login/>
        },
        {
          path: "registrazione",
          element: <Register/>,
        },
        {
          path: "main",
          element: <Main/>
        }
      ]
    }
  ]
)

ReactDOM.createRoot(document.getElementById('root')).render(
  <RouterProvider router={router}/>
)
