package it.meteo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeteoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeteoBackendApplication.class, args);
	}

}
