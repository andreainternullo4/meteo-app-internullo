package it.meteo.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import it.meteo.model.Ruolo;

public interface RuoloDao extends CrudRepository<Ruolo, Integer>{

	Optional<Ruolo> findById(int id);
		
}
