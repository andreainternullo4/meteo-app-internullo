/**
 * 
 */
package it.meteo.dao;

import org.springframework.data.repository.CrudRepository;

import it.meteo.model.Utente;

/**
 * 
 */
public interface UtenteDao extends CrudRepository<Utente, Integer>{
	 
	boolean existsByEmail(String email);

	Utente findByEmailAndPassword(String email, String password);
	
	Utente findByEmail(String Utente);
}
