package it.meteo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import it.meteo.model.Informazioni;

public interface InformazioniDao extends JpaRepository<Informazioni, Integer>{

}
