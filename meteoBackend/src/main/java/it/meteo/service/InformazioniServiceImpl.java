package it.meteo.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.meteo.dao.InformazioniDao;
import it.meteo.dto.InformazioniDto;
import it.meteo.model.Informazioni;

@Service
public class InformazioniServiceImpl implements InformazioniService{
	
	@Autowired
	private InformazioniDao informazioniDao;
	
	private ModelMapper mapper = new ModelMapper();

	@Override
	public Informazioni aggiungiInformazioni(InformazioniDto info) {
		
		Informazioni newInfo = new Informazioni();
		
		newInfo = mapper.map(info, Informazioni.class);
		newInfo = informazioniDao.save(newInfo);
		
		return newInfo;
	}

}
