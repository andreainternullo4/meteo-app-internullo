package it.meteo.service;

import it.meteo.dto.InformazioniDto;
import it.meteo.model.Informazioni;

public interface InformazioniService {

		Informazioni aggiungiInformazioni(InformazioniDto info);
}
