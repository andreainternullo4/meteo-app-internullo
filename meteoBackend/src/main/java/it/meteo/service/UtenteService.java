package it.meteo.service;

import it.meteo.dto.UtenteLoginRequestDto;
import it.meteo.dto.UtenteRegistrazioneDto;
import it.meteo.model.Utente;

public interface UtenteService {
	
	Utente findByEmail(String email);
	
	boolean existsUtenteByEmail(String email);
	
	// Metodo per la registrazione dell'utente
	void userRegistration(UtenteRegistrazioneDto utenteDto);
	
	// Metodo per effettuare il login
	boolean loginUtente(UtenteLoginRequestDto utente);
}
