package it.meteo.dto;

public class InformazioniDto {
	
	
	private String nomecitta;
	
	
	private String stato;
	
	
	private String regione;
	
	
	private Double gradi;
	
	
	private Double gradipercepiti;
	
	
	private Integer umidita;
	

	private String descrizione;
	
	
	private Double visibilita;
	
	
	private Double uv;
	

	private Integer pressione;
	

	private Double vento;
	
	private Integer idutente;
	

	public String getNomecitta() {
		return nomecitta;
	}

	public void setNomeCitta(String nomecitta) {
		this.nomecitta = nomecitta;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getRegione() {
		return regione;
	}

	public void setRegione(String regione) {
		this.regione = regione;
	}

	public Double getGradi() {
		return gradi;
	}

	public void setGradi(Double gradi) {
		this.gradi = gradi;
	}

	public Double getGradipercepiti() {
		return gradipercepiti;
	}

	public void setGradipercepiti(Double gradiPercepiti) {
		this.gradipercepiti = gradiPercepiti;
	}

	public Integer getUmidita() {
		return umidita;
	}

	public void setUmidita(Integer umidita) {
		this.umidita = umidita;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Double getVisibilita() {
		return visibilita;
	}

	public void setVisibilita(Double visibilita) {
		this.visibilita = visibilita;
	}

	public Double getUv() {
		return uv;
	}

	public void setUv(Double uv) {
		this.uv = uv;
	}

	public Integer getPressione() {
		return pressione;
	}

	public void setPressione(Integer pressione) {
		this.pressione = pressione;
	}

	public Double getVento() {
		return vento;
	}

	public void setVento(Double vento) {
		this.vento = vento;
	}

	public Integer getIdutente() {
		return idutente;
	}

	public void setIdutente(Integer idutente) {
		this.idutente = idutente;
	}
	
	
}
