/**
 * 
 */
package it.meteo.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

/**
 * 
 */
@Entity
@Table(name = "informazioni")
public class Informazioni {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "id" )
	private int id;
	
	@Column( name = "nomecitta" )
	private String nomecitta;
	
	@Column( name = "stato" )
	private String stato;
	
	@Column( name = "regione" )
	private String regione;
	
	@Column( name = "gradi" )
	private Double gradi;
	
	@Column( name = "gradipercepiti" )
	private Double gradipercepiti;
	
	@Column( name = "umidita" )
	private Integer umidita;
	
	@Column( name = "descrizione" )
	private String descrizione;
	
	@Column( name = "visibilita" )
	private Double visibilita;
	
	@Column( name = "uv" )
	private Double uv;
	
	@Column( name = "pressione" )
	private Integer pressione;
	
	@Column( name = "vento" )
	private Double vento;
	
	@ManyToOne( cascade = CascadeType.REFRESH )
	@JoinColumn( name = "idutente", referencedColumnName = "id" )
	private Utente utente;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomecitta() {
		return nomecitta;
	}

	public void setNomecitta(String nomecitta) {
		this.nomecitta = nomecitta;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getRegione() {
		return regione;
	}

	public void setRegione(String regione) {
		this.regione = regione;
	}

	public Double getGradi() {
		return gradi;
	}

	public void setGradi(Double gradi) {
		this.gradi = gradi;
	}

	public Double getGradipercepiti() {
		return gradipercepiti;
	}

	public void setGradiPercepiti(Double gradipercepiti) {
		this.gradipercepiti = gradipercepiti;
	}

	public Integer getUmidita() {
		return umidita;
	}

	public void setUmidita(Integer umidita) {
		this.umidita = umidita;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Double getVisibilita() {
		return visibilita;
	}

	public void setVisibilita(Double visibilita) {
		this.visibilita = visibilita;
	}

	public Double getUv() {
		return uv;
	}

	public void setUv(Double uv) {
		this.uv = uv;
	}

	public Integer getPressione() {
		return pressione;
	}

	public void setPressione(Integer pressione) {
		this.pressione = pressione;
	}

	public Double getVento() {
		return vento;
	}

	public void setVento(Double vento) {
		this.vento = vento;
	}

	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}
	
	
	
}
