CREATE DATABASE meteo; 

USE meteo;

CREATE TABLE utente (
	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL,
    cognome VARCHAR(50) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE, CONSTRAINT check_email CHECK(email LIKE '%@%.%'),
    `password` VARCHAR(255) NOT NULL,
    idRuolo INTEGER, CONSTRAINT fk_id_ruolo_utente FOREIGN KEY (idRuolo) REFERENCES ruolo(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(id)
);

CREATE TABLE ruolo (
	id INT PRIMARY KEY AUTO_INCREMENT,
    tipologia VARCHAR(255)
);

CREATE TABLE informazioni (
    id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    nomeCitta VARCHAR(50) NOT NULL,
    stato VARCHAR(2) NOT NULL,
    regione VARCHAR(100) NOT NULL,
    gradi DOUBLE NOT NULL,
    gradiPercepiti DOUBLE NOT NULL,
    umidita INT,
    descrizione TEXT NOT NULL,
    visibilita DOUBLE,
    uv DOUBLE,
    pressione INT,
    vento DOUBLE,
    PRIMARY KEY (id),
    idUtente INTEGER, CONSTRAINT fk_id_utente_info FOREIGN KEY (idUtente) REFERENCES utente(id)
);